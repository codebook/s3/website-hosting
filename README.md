# Static Website Hosting on Amazon S3


_TBD_



## References

* [Hosting a Static Website on Amazon S3](http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
* [How Do I Configure an S3 Bucket for Static Website Hosting?](http://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html)
* [Example: Setting up a Static Website Using a Custom Domain](http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html)

